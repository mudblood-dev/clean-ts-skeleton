import { routeController } from 'utilities';
import { users } from '../../use-cases';
import { handleError } from '../../utils';

interface ReqBody {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}

export function makeCreateUserAction(
  createUser: users.CreateUser
): routeController {
  return async function createUserAction(req) {
    const { email, firstName, lastName, password } = req.body as ReqBody;
    try {
      const user = await createUser({
        logger: req.logger,
        email,
        firstName,
        lastName,
        password,
      });
      return {
        data: user,
        statusCode: 201,
      };
    } catch (err) {
      return handleError(err);
    }
  };
}
