import { users } from '../../use-cases';

import { makeCreateUserAction } from './create-user-action';
const createUserAction = makeCreateUserAction(users.createUser);

export { createUserAction };
