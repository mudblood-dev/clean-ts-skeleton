export enum Environment {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development',
  LOCAL = 'local',
}
export function getNodeEnv() {
  return (process.env.ENVIRONMENT as Environment) || Environment.LOCAL;
}
