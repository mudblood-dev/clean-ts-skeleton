import { RouteControllerResponse } from 'utilities';
import { HTTPError, CustomError } from '../errors';

export function handleError(err: unknown) {
  const returnObj: RouteControllerResponse = {
    status: 'error',
    statusCode: 500,
    message: 'Something went wrong.',
  };
  if (err instanceof Error) {
    returnObj.message = err.message;
    if (err instanceof CustomError) {
      returnObj.customErrorCode = err.customErrorCode;
      if (err instanceof HTTPError) {
        returnObj.statusCode = err.statusCode;
      }
    }
  }
  return returnObj;
}
