import { CockroachDbUtil, CockroachDBError, Logger } from 'utilities';
import { UnknownError } from '../errors';
import { BasicSchema } from './types';

export interface UserSchema extends BasicSchema {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
}
export const makeUserDataSource = (pool: CockroachDbUtil) => {
  async function getUsers(args: { logger: Logger }) {
    const { logger } = args;
    const query = 'SELECT * FROM users';

    try {
      const result = await pool.executeQuery<UserSchema>({
        query,
      });
      return result.rows;
    } catch (err) {
      logger.error('makeUserDataSource: getUsers');
      if (err instanceof CockroachDBError) {
        const { code, name, message } = err;
        logger.debug({ code, name, message });
      }
      throw new UnknownError('Something went wrong');
    }
  }
  async function getUser(args: { id: string; logger: Logger }) {
    const { logger, id } = args;
    const values = [id];
    const query = 'SELECT * FROM users WHERE id = $1';
    try {
      const result = await pool.executeQuery<UserSchema>({
        query,
        values,
      });
      return result.rows[0];
    } catch (err) {
      logger.error('makeUserDataSource: getUsers');
      if (err instanceof CockroachDBError) {
        const { code, name, message } = err;
        logger.debug({ code, name, message });
      }
      throw new UnknownError('Something went wrong');
    }
  }
  async function createUser(args: {
    user: Omit<UserSchema, 'created_at' | 'updated_at' | 'id'>;
    logger: Logger;
  }): Promise<UserSchema> {
    const { user, logger } = args;
    const values = [user.first_name, user.last_name, user.email, user.password];
    const query =
      'INSERT INTO users (first_name, last_name, email, password) VALUES ($1, $2, $3, $4) RETURNING *';
    try {
      const result = await pool.executeQuery<UserSchema>({
        query,
        values,
      });
      return result.rows[0];
    } catch (err) {
      logger.error('makeUserDataSource: getUsers');
      if (err instanceof CockroachDBError) {
        const { code, name, message } = err;
        logger.debug({ code, name, message });
      }
      throw new UnknownError('Something went wrong');
    }
  }
  async function deleteUser(args: {
    id: string;
    logger: Logger;
  }): Promise<UserSchema> {
    const { id, logger } = args;
    const values = [id];
    const query = 'DELETE FROM users WHERE id = $1 RETURNING *';
    try {
      const result = await pool.executeQuery<UserSchema>({
        query,
        values,
      });
      return result.rows[0];
    } catch (err) {
      logger.error('makeUserDataSource: getUsers');
      if (err instanceof CockroachDBError) {
        const { code, name, message } = err;
        logger.debug({ code, name, message });
      }
      throw new UnknownError('Something went wrong');
    }
  }
  return {
    getUsers,
    createUser,
    deleteUser,
    getUser,
  };
};
