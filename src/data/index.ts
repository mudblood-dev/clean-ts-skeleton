import { CockroachDbUtil } from 'utilities';

import config from '../config';

const cockroachDb = new CockroachDbUtil(config.backendConfig.cockroachDB);

import { makeUserDataSource } from './user';
const userDataSource = makeUserDataSource(cockroachDb);
type UserDataSource = typeof userDataSource;

export { userDataSource };

export { UserDataSource };
