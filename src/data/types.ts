export interface BasicSchema {
  id: string | number;
  created_at: string;
  updated_at: string;
}
