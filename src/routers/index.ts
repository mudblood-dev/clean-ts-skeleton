import { Router } from 'utilities';

import userRouter from './user';

const baseRouter = new Router('/v1');

baseRouter.addChildRouter(userRouter);

export default baseRouter;
