import { Router } from 'utilities';

import { userControllers } from '../controllers';

const router = new Router('/users');

router.post({
  path: '',
  controller: userControllers.createUserAction,
});

export default router;
