export type ServiceEndpointConfig = Record<string, string>;

export const config: ServiceEndpointConfig = {};
