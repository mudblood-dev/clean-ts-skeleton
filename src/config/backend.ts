import { CockroachDbConfig, LoggingConfig } from 'utilities';

export interface BackendConfig {
  cockroachDB: CockroachDbConfig;
  logging: LoggingConfig;
}
export const config: BackendConfig = {
  cockroachDB: {
    host: 'localhost',
    port: 26257,
    username: 'root',
    password: 'root',
    database: 'sampledb',
  },
  logging: {
    console: {
      enabled: true,
    },
  },
};
