import { config as backendConfig } from './backend';
import { config as serviceEndpoints } from './service-endpoints';
import { config as serviceConfig } from './service-config';

import { Options as SequelizeConfig } from 'sequelize';

export interface MigrationsConfig {
  cockroachDB: SequelizeConfig;
}

const migrationsConfig: MigrationsConfig = {
  cockroachDB: {
    dialect: 'postgres',
    port: backendConfig.cockroachDB.port,
    host: backendConfig.cockroachDB.host,
    pool: {
      max: 10,
      min: 0,
      idle: 1000,
    },
    dialectOptions: {
      multipleStatements: true,
      decimalNumbers: true,
    },
    omitNull: false,
    username: backendConfig.cockroachDB.username,
    password: backendConfig.cockroachDB.password as string,
    database: 'defaultdb',
  },
};

const config = {
  backendConfig: backendConfig,
  serviceEndpoints: serviceEndpoints,
  migrationsConfig,
  ...serviceConfig,
};

export type Config = typeof config;

export default config;
