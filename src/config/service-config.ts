export interface ServiceConfig {
  serviceName: string;
  secretFields: {
    logging: string[];
    response: string[];
  };
}
export const config: ServiceConfig = {
  serviceName: 'sample-service',
  secretFields: {
    logging: ['password'],
    response: [],
  },
};
