// Declare your custom utility type in the global scope
declare global {
  type MakeKeysOptional<T, K extends keyof T> = Omit<T, K> &
    Partial<Pick<T, K>>;
}

import { createLogger } from 'utilities';
import Server from 'patronus-server';

import baseRouter from './routers';
import config from './config';

const server = new Server({
  logger: createLogger(config.backendConfig?.logging),
  serviceName: config.serviceName,
  secretFields: config.secretFields,
});

server.start(baseRouter);
