import { CustomError, CustomErrorArgs } from './custom-error';

export interface HTTPErrorArgs extends CustomErrorArgs {
  statusCode: number;
}

export class HTTPError extends CustomError {
  statusCode: number;

  constructor(args: HTTPErrorArgs) {
    super({
      message: args.message,
      name: args.name,
      customErrorCode: args.customErrorCode,
      metadata: args.metadata,
    });
    this.statusCode = args.statusCode;
  }

  getStatusCode() {
    return this.statusCode;
  }
}
