import { HTTPError } from './http-error';

export class UnknownError extends HTTPError {
  constructor(message: string) {
    super({
      message,
      name: 'UnknownError',
      statusCode: 500,
    });
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownError);
    }
  }
}
