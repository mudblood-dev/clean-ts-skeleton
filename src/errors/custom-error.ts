export interface CustomErrorArgs {
  name: string;
  message: string;
  metadata?: Record<string, unknown>;
  customErrorCode?: string;
}
export class CustomError extends Error {
  customErrorCode?: string;

  metadata: Record<string, unknown>;

  constructor(args: CustomErrorArgs) {
    super(args.message);
    this.name = args.name;
    this.metadata = args.metadata || {};
    this.customErrorCode = args.customErrorCode;
  }

  getErrorCode() {
    return this.customErrorCode;
  }

  getMetadata() {
    return this.metadata;
  }
}
