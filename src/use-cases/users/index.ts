import { userDataSource } from '../../data';
import { makeCreateUser } from './create-user';

const createUser = makeCreateUser({
  userDataSource,
});
type CreateUser = typeof createUser;
export { createUser, CreateUser };
