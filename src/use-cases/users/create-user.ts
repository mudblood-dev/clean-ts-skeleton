import { BasicUseCaseArgs } from '../types';
import { UserDataSource } from '../../data';

interface CreateUserArgs extends BasicUseCaseArgs {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}

interface MakeArgs {
  userDataSource: UserDataSource;
}

export function makeCreateUser(makeArgs: MakeArgs) {
  const { userDataSource } = makeArgs;
  return async function createUser({
    logger,
    email,
    firstName,
    lastName,
    password,
  }: CreateUserArgs) {
    const createdUser = await userDataSource.createUser({
      logger,
      user: {
        email,
        first_name: firstName,
        last_name: lastName,
        password,
      },
    });
    return createdUser;
  };
}
