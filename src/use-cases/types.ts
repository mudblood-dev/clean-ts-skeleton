import { Logger } from 'utilities';
export interface BasicUseCaseArgs {
  logger: Logger;
}
