/**
 * @typedef {import('sequelize').Sequelize} Sequelize
 */
'use strict';

const { DataTypes } = require('sequelize');

module.exports = {
  /**
   * @param {{context: Sequelize}} {context}
   */
  up: async ({ context: sequelize }) => {
    const queryInterface = sequelize.getQueryInterface();
    // Migration logic here
    await queryInterface.createTable('users', {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: sequelize.literal('gen_random_uuid()'),
      },
      first_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      last_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      created_at: {
        type: 'TIMESTAMPTZ',
        defaultValue: sequelize.literal('NOW()'),
      },
      updated_at: {
        type: 'TIMESTAMPTZ',
        defaultValue: sequelize.literal('NOW()'),
      },
    });
  },
  /**
   * @param {QueryInterface} queryInterface
   * @param {Sequelize} Sequelize
   */
  down: async (queryInterface, Sequelize) => {
    // Rollback logic here
  },
};
