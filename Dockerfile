FROM node:18-alpine AS builder
WORKDIR /app
RUN apk add git python3 make g++

# Cache and Install dependencies
COPY package.json .
COPY package-lock.json .
RUN npm install

# Run necessary files
COPY . .

# Build the app
RUN npm run build

EXPOSE 8080

CMD ["sh", "-c", "npm run run-sequelize-migrations --skip-build && npm start"]