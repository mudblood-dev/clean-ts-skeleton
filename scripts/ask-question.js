const readline = require('readline');

module.exports = function askQuestion(question, choices = []) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise((resolve) => {
    if (choices.length === 0) {
      rl.question(question, (answer) => {
        rl.close();
        resolve(answer);
      });
    } else {
      console.log(question);
      console.log();
      choices.forEach((choice, index) => {
        console.log(`${index + 1}. ${choice}`);
      });
      console.log();

      rl.question('Enter the number of your choice: ', (answer) => {
        const selectedIndex = parseInt(answer, 10) - 1;
        if (selectedIndex >= 0 && selectedIndex < choices.length) {
          rl.close();
          resolve(choices[selectedIndex]);
        } else {
          console.log('Invalid choice. Please select a valid number.');
          rl.close();
          resolve(askQuestion(question, choices));
        }
      });
    }
  });
};
