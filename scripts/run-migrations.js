const path = require('path');
const { Sequelize } = require('sequelize');
const { Umzug, SequelizeStorage } = require('umzug');

let config = require('./get-config')();


if (!config) {
  console.error('No config found');
  process.exit(1);
}
const { migrationsConfig, backendConfig } = config;

(async () => {
  // Checks migrations and run them if they are not already applied. To keep
  // track of the executed migrations, a table (and sequelize model) called SequelizeMeta
  // will be automatically created (if it doesn't exist already) and parsed.
  if (!migrationsConfig) {
    console.error('No migrations config found');
    process.exit(1);
  }

  for (const dialect of Object.keys(config.migrationsConfig)) {
    let sequelize = new Sequelize(config.migrationsConfig[dialect]);

    const dbExists = await createDatabase(
      sequelize,
      backendConfig[dialect].database
    );
    if (!dbExists) {
      console.error('Could not create database');
      process.exit(1);
    }

    sequelize = new Sequelize({
      ...config.migrationsConfig[dialect],
      database: backendConfig[dialect].database,
    });

    const umzug = new Umzug({
      migrations: {
        glob: path.join(__dirname, '..', `migrations/${dialect}/*.js`),
      },
      context: sequelize,
      storage: new SequelizeStorage({ sequelize }),
      logger: console,
    });
    await umzug.up();
  }
})();

// Function to create the database
async function createDatabase(sequelize, dbName) {
  try {
    await sequelize.authenticate();
    await sequelize.query(`CREATE DATABASE ${dbName}`);
    return true;
  } catch (error) {
    if (error.original.code === '42P04') {
      return true;
    } else {
      console.log(error);
      return false;
    }
  }
}
