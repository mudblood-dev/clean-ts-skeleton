module.exports = function () {
  const env = process.env.NODE_ENV || 'local';

  let config;

  switch (env) {
    case 'local': {
      const { existsSync } = require('fs');
      const path = require('path');
      const buildFolder = path.join(__dirname, '..', 'build');
      if (!existsSync(buildFolder)) {
        const { execSync } = require('child_process');
        execSync('npm run build', { cwd: path.join(__dirname, '..') });
      }
      config = require(path.join(__dirname, '..', 'build', 'config'));
      break;
    }
    case 'development':
    case 'production': {
      config = require(path.join(__dirname, '..', 'src', 'config'));
      break;
    }
    default: {
      throw new Error(`Unknown environment: ${env}`);
    }
  }
  return config.default;
};
