const fs = require('fs');
const path = require('path');
const askQuestion = require('./ask-question');
const migrationName = process.argv[2];

if (!migrationName) {
  console.error('Please provide a migration name.');
  process.exit(1);
}

// Enforce specific naming format
if (
  !/^(create|alter|drop)_(table|db|index|constraint)_[a-zA-Z0-9_]+$/.test(
    migrationName
  )
) {
  console.error(
    'Invalid migration name format. Please use {create/alter/drop}_{table/db/index/constraint}_{name}.'
  );
  process.exit(1);
}

let config = require('./get-config')();

if (!config) {
  console.error('No config found');
  process.exit(1);
}
const { migrationsConfig, backendConfig } = config;
(async () => {
  const dialect =
    process.env.MIGRATION_DIALECT ||
    (await askQuestion(
      'Please choose a dialect:',
      Object.keys(migrationsConfig)
    ));
  // Create a timestamp for the migration filename
  const timestamp = new Date().toISOString().replace(/[-:.]/g, '');

  // Sequelize migration file template
  const migrationTemplate = `
  /**
   * @typedef {import('sequelize').Sequelize} Sequelize
   */
  'use strict';
  
  const { DataTypes } = require('sequelize');
  
  /**
   * @typedef {{
   *  up: (options: {context: Sequelize}) => Promise<void>,
   *  down: (options: {context: Sequelize}) => Promise<void>
   * }} Output
   */
  /**
   * @type {Output}
   */
  const output = {
    up: async ({ context: sequelize }) => {
      const queryInterface = sequelize.getQueryInterface();
      // Migration logic here
    },
    down: async ({ context: sequelize }) => {
      const queryInterface = sequelize.getQueryInterface();
      // Rollback logic here
    },
  };
  module.exports = output;
  
  `;

  const migrationFolderPath = path.join(__dirname, '../migrations');
  if (!fs.existsSync(migrationFolderPath)) {
    fs.mkdirSync(migrationFolderPath);
  }

  const dialectFolderPath = path.join(migrationFolderPath, dialect);
  if (!fs.existsSync(dialectFolderPath)) {
    fs.mkdirSync(dialectFolderPath);
  }

  // Create the migration file
  const migrationFileName = `${timestamp}-${migrationName}.js`;
  const migrationFilePath = path.join(dialectFolderPath, migrationFileName);

  fs.writeFileSync(migrationFilePath, migrationTemplate);
  console.log(`Migration file "${migrationFileName}" created successfully.`);
})();
